export default {
	state: {
		messages: [
			{
				user: 'Broks',
				message: 'Инструмент маркетинга экономит культурный анализ зарубежного опыта, оптимизируя бюджеты',
			},
			{
				user: 'Ganesh',
				message: 'Тем не менее, план размещения притягивает повторный контакт.',
			},

		]

	},
	getters: {
		GET_MESSAGES: state => {
			return state.messages
		}
	},
	actions: {
		POST_MESSAGE: (context, payload) => {
			context.commit('POST_MESSAGE', payload)
		}
	},
	mutations: {
		POST_MESSAGE: (state, payload) => {
			state.messages.push(payload)
		}
	}
}